import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LaunchO2HomePageMethod'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_The O2 Wifi/button_Head in'))

WebUI.waitForElementVisible(findTestObject('Page_The O2 Wifi/button_a-hamburger'), 5000, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_The O2 Wifi/button_a-hamburger'), 5000, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Page_The O2 Wifi/button_a-hamburger'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_The O2 Wifi/a_Whats on today'))

WebUI.verifyTextPresent('Welcome to', true)

'Assert the \'Climb the roof of this world\' image is present'
WebUI.verifyElementPresent(findTestObject('Page_The O2 Wifi/climbTheRoofImage'), 0)

//WebUI.click(findTestObject('Page_The O2 Wifi/Climb the roof Link'))
'Assert  \'Cloakroom on the house\' image is present'
WebUI.verifyElementPresent(findTestObject('Page_The O2 Wifi/cloakRoomImage'), 0)

'Assert  \'Cloakroom on the house\' Text is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/cloakRoomText'), 0)

'Assert  \'O2 Lounge\' Image is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2LoungeImage'), 0)

'Assert  \'O2 Lounge\' Text is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2LoungeText'), 0)


'Assert  \'O2 Blue Room\' Image is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2BlueRoomImage'), 0)

'Assert  \'O2 Blue Room\' Text is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2BlueRoomText'), 0)

'Assert  \')2 Map\' Image is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2BlueRoomImage'), 0)

'Assert  \')2 Map\' Text is present'
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The O2 Wifi/o2BlueRoomText'), 0)


WebUI.closeBrowser()

