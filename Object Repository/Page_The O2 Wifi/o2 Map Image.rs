<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>o2 Map Image</name>
   <tag></tag>
   <elementGuidId>9736e43f-a2f9-4b3e-98ea-2bc33b384a40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[contains(@src, '/static/img/map')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/static/img/map</value>
   </webElementProperties>
</WebElementEntity>
