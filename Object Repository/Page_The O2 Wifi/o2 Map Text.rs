<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>o2 Map Text</name>
   <tag></tag>
   <elementGuidId>c8684482-d471-42a5-b1ef-4cb1463698a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Find your way around' or . = 'Find your way around')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Find your way around</value>
   </webElementProperties>
</WebElementEntity>
