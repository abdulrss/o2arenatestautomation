<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>upcoming Events Text</name>
   <tag></tag>
   <elementGuidId>def33040-f4da-4f5a-a2b0-a51a8e3adc7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Upcoming events you might like' or . = 'Upcoming events you might like')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Upcoming events you might like</value>
   </webElementProperties>
</WebElementEntity>
