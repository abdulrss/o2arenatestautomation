<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Head in</name>
   <tag></tag>
   <elementGuidId>201a29c5-839a-49e8-936e-3f082c79c10c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Head in' or . = 'Head in')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>a-button a-button--medium a-button--blue a-button--text-center</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Head in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;#modals-container&quot;)/div[@class=&quot;v--modal-overlay&quot;]/div[@class=&quot;v--modal-background-click&quot;]/div[@class=&quot;v--modal-box v--modal&quot;]/article[@class=&quot;m-welcome-modal&quot;]/div[@class=&quot;m-welcome-modal__ctas&quot;]/button[@class=&quot;a-button a-button--medium a-button--blue a-button--text-center&quot;]</value>
   </webElementProperties>
</WebElementEntity>
