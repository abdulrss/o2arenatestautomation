<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Climb the roof of this wo</name>
   <tag></tag>
   <elementGuidId>e79309cd-99a2-4469-8e01-c3fcac5e828c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>a-button__text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Climb the roof of this world-famous venue </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;router-container&quot;]/div[@class=&quot;t-home&quot;]/div[@class=&quot;t-home__container u-container u-horizontal-padding u-grid&quot;]/section[@class=&quot;t-home__section u-grid t-home__intro&quot;]/article[@class=&quot;m-intro&quot;]/div[@class=&quot;m-intro__event-meta&quot;]/div[@class=&quot;a-full-width-wrapper a-full-width-wrapper--navy&quot;]/a[@class=&quot;a-button a-button--medium a-button--blue-light a-button--text-center&quot;]/span[@class=&quot;a-button__text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
