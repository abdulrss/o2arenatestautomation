<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>climbTheRoofImage</name>
   <tag></tag>
   <elementGuidId>95c1bdb7-6d3b-4fbe-80e7-c523a568129a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[contains(@src, '/static/img/up-at-the-o2')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/static/img/up-at-the-o2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;router-container&quot;]/div[@class=&quot;t-home&quot;]/div[@class=&quot;t-home__container u-container u-horizontal-padding u-grid&quot;]/section[@class=&quot;t-home__section u-grid t-home__intro&quot;]/article[@class=&quot;m-intro&quot;]/div[@class=&quot;m-intro__image u-hero-image&quot;]/div[@class=&quot;a-single-image a-single-image--object-fit a-single-image--is-loaded&quot;]/img[1]</value>
   </webElementProperties>
</WebElementEntity>
