<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>o2LoungeText</name>
   <tag></tag>
   <elementGuidId>226d0028-59c7-4603-ba66-494d3db40103</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'O2 Lounge' or . = 'O2 Lounge')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>O2 Lounge</value>
   </webElementProperties>
</WebElementEntity>
