<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>o2BlueRoomText</name>
   <tag></tag>
   <elementGuidId>c2a76dff-4ccc-45af-a02e-c25360731ffe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'O2 Lounge' or . = 'O2 Lounge')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>O2 Lounge</value>
   </webElementProperties>
</WebElementEntity>
