<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Climb the roof Link</name>
   <tag></tag>
   <elementGuidId>09a8de72-b0fb-4264-9c9c-6355707f394a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>link</name>
      <type>Main</type>
      <value>Climb the roof of this world-famous venue</value>
   </webElementProperties>
</WebElementEntity>
